library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;

	-- Add your library and packages declaration here ...

entity reg_tb is
	-- Generic declarations of the tested unit
		generic(
		default : INTEGER := 4;
		seq_time : TIME := 15.0 ns;
		par_time : TIME := 20.0 ns );
end reg_tb;

architecture TB_ARCHITECTURE of reg_tb is
	-- Component declaration of the tested unit
	component reg
		generic(
		default : INTEGER := 4;
		seq_time : TIME := 15.0 ns;
		par_time : TIME := 20.0 ns );
	port(
		C : in STD_LOGIC;
		Mode : in STD_LOGIC;
		Parallel_In : in STD_LOGIC_VECTOR(default-1 downto 0);
		Serial_In : in STD_LOGIC;
		Parallel_Out : out STD_LOGIC_VECTOR(default-1 downto 0);
		Serial_Out : out STD_LOGIC );
	end component;

	-- Stimulus signals - signals mapped to the input and inout ports of tested entity
	signal C : STD_LOGIC;
	signal Mode : STD_LOGIC;
	signal Parallel_In : STD_LOGIC_VECTOR(default-1 downto 0);
	signal Serial_In : STD_LOGIC;
	-- Observed signals - signals mapped to the output ports of tested entity
	signal Parallel_Out : STD_LOGIC_VECTOR(default-1 downto 0);
	signal Serial_Out : STD_LOGIC;

	-- Add your code here ... 
	constant clk_period : time := 100 ns;	
	constant parallel_start: integer := 0; 
	constant parallel_period: time := 45 ns;  


begin

	-- Unit Under Test port map
	UUT : reg
		generic map (
			default => default,
			seq_time => seq_time,
			par_time => par_time
		)

		port map (
			C => C,
			Mode => Mode,
			Parallel_In => Parallel_In,
			Serial_In => Serial_In,
			Parallel_Out => Parallel_Out,
			Serial_Out => Serial_Out
		);

	-- Add your stimulus here ...
	clk_process: process
	begin
		C <='0';
		wait for clk_period/2;
		C <= '1';
		wait for clk_period/2;
	end process;	 
	
	parallel_load_process: process	 
	begin		
		Parallel_In <= std_logic_vector(to_unsigned(parallel_start, default));
		loop 
			wait for parallel_period;	 
			Parallel_In <= std_logic_vector(unsigned(Parallel_In) + 1);	
			exit when now > 1000ns;
		end loop;
		Parallel_In <= std_logic_vector(to_unsigned(parallel_start, default));
		wait;
	end process;	
	
	stimulus_process : process
	begin	 
		Serial_In <='1';
		Mode <= '1';
		wait for 1000ns; 
		Mode <= '0';
		wait for clk_period;
		Serial_In <= '0';
		wait for clk_period;
		Serial_In <= '1';	 
		wait for 500ns;
		Serial_In <= 'X';
		wait;
	end process;
		
		
	
	
end TB_ARCHITECTURE;

configuration TESTBENCH_FOR_reg of reg_tb is
	for TB_ARCHITECTURE
		for UUT : reg
			use entity work.reg(arc);
		end for;
	end for;
end TESTBENCH_FOR_reg;

