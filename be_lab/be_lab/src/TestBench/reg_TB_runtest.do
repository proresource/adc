SetActiveLib -work
comp -include "$dsn\src\reg.vhd" 
comp -include "$dsn\src\TestBench\reg_TB.vhd" 
asim +access +r TESTBENCH_FOR_reg 
wave 
wave -noreg C
wave -noreg Mode
wave -noreg Parallel_In
wave -noreg Serial_In
wave -noreg Parallel_Out
wave -noreg Serial_Out
# The following lines can be used for timing simulation
# acom <backannotated_vhdl_file_name>
# comp -include "$dsn\src\TestBench\reg_TB_tim_cfg.vhd" 
# asim +access +r TIMING_FOR_reg 
