													 library ieee;
use ieee.std_logic_1164.all;

entity reg is 				 
	generic(default:integer:=4; 
	seq_time: time:=15ns; 
	par_time: time:=20ns);
	port (C, Mode : in std_logic;
	Parallel_In : in std_logic_vector(default-1 downto 0);
	Serial_In : in std_logic;
	Parallel_Out : out std_logic_vector (default-1 downto 0); 
	Serial_Out : out std_logic);
end reg;

architecture arc of reg is 
signal temp: std_logic_vector(default-1 downto 0);
begin
	process(C)
	begin
		if(C'event and C='1') then 
			if ((Serial_In = '0' or Serial_In = '1') and (Mode='1' or Mode='0')) then
				if (Mode = '1') then 			 
					temp(default-1 downto 0) <= transport Parallel_In(default-1 downto 0) after par_time;
					
				elsif (Mode = '0') then 
					temp(default-2 downto 0) <= transport temp(default-1 downto 1) after par_time;
					temp(default-1) <= transport Serial_In after seq_time;
				end if;
			else 
				temp <= (others => 'X');
			end if;
		end if;	   
	end process;
	
	Serial_Out <= temp(default-1);
	Parallel_Out(default-1 downto 0) <= temp(default-1 downto 0);
end arc;
			