-------------------------------------------------------------------------------
--
-- Title       : DC
-- Design      : l1
-- Author      : pr
-- Company     : be
--
-------------------------------------------------------------------------------
--
-- File        : DC.vhd
-- Generated   : Thu Mar 28 12:51:45 2013
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.20
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {DC} architecture {DC}}

library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity DC is
	 port(
		 x : in BIT_VECTOR(1 downto 0);
		 y : out BIT_VECTOR(3 downto 0)
	     );
end DC;

--}} End of automatically maintained section

architecture DC of DC is
begin
	y<="0001" when x="00" else
		"0010" when x="01" else
		"0100" when x="10" else
		"1000" when x="11";
	 -- enter your statements here --

end DC;
