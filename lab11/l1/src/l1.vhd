	   -------------------------------------------------------------------------------
--
-- Title       : be
-- Design      : lab1
-- Author      : pr
-- Company     : be
--
-------------------------------------------------------------------------------
--
-- File        : be.vhd
-- Generated   : Tue Feb 19 16:41:14 2013
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.20
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {be} architecture {be}}

library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity be is
	 port(
		 a, b, c : in BIT;
		 d, e : out BIT
	     );
end be;

--}} End of automatically maintained section

architecture be of be is
begin
   d<=(not a) or (b and c);
   e<=(a and b and c) or ((not c)and a);
	 -- enter your statements here --

end be;
