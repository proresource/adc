library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity DC is                                      
        port(
                X : in  BIT_VECTOR(1 downto 0);
                Y : out BIT_VECTOR(3 downto 0) 
        );
end DC;

architecture behavior of DC is
begin   
        process (X) begin
                case X is
                        when "00" => Y <= "0001";
                        when "01" => Y <= "0010";
                        when "10" => Y <= "0100";
                        when "11" => Y <= "1000";
                end case;
        end process;
end behavior;           