library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity be is
	 port(
		 a, b, c : in BIT;
		 d, e : out BIT
	     );
end be;

--}} End of automatically maintained section

architecture be of be is
begin
   d<=a or (b and c);
   e<=(((not a ) and (not b)) and (not c )) or a;
	 -- enter your statements here --

end be;
