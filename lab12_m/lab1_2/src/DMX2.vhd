library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity DMX2 is
	 port(
		 D : in BIT;
		 A : in BIT_VECTOR(1 downto 0);
		 y : inout BIT_VECTOR(3 downto 0)
	     );
end DMX2;

architecture DMX2 of DMX2 is
begin
	with A select
	 y <="000"&not(D) when "11",
	  "00"&not(D)&"0" when "10",
	  "0"&not(D)&"00" when "01",
	  not(D)&"000" when "00";
end DMX2;

