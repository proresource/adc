library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity and2 is
        generic(t_hold: time := 0 ns);
        port (
			in1, in2        : in    STD_LOGIC;
            out1            : inout STD_LOGIC
		);
end and2;

architecture default of and2 is   
begin
        out1 <= in1 and in2 after t_hold;
end default;