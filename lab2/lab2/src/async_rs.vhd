library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity async_rs is
        port(
        	notS, notR  	: in    STD_LOGIC;
        	Q, notQ                 		: inout STD_LOGIC
        );
end async_rs;

architecture default of async_rs is   
begin                                                           	
	nand1 : entity nand2(default)	
		port map(notS, notQ, Q);	 
	nand2 : entity nand2(default)
		port map(notR, Q, notQ);
end default;				  

architecture component_behavior of async_rs is
			
	component gate_nand2
		port(
		in1, in2		: in std_logic;
		out1			: out std_logic
		);
	end component;
	
begin
	dd1 : gate_nand2 port map(notS, notQ,Q);
	dd2 : gate_nand2 port map(notR, Q,notQ);
end component_behavior;