library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity invertor is
	 port(
		 input : in STD_LOGIC;
		 output : out STD_LOGIC
	     );
end invertor;

architecture invertor of invertor is
begin
		  output <=not input;
end invertor;
