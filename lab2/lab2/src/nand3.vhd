library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity nand3 is
        generic(t_hold: time := 0 ns);
        port(
        	in1, in2, in3	: in    STD_LOGIC;
            out1            : inout STD_LOGIC
        );
end nand3;

architecture default of nand3 is   
begin                                                           
        out1 <= not(in1 and in2 and in3) after t_hold;
end default;