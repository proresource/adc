library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity RSMS is                                      
        port(
        	S, R, C 	: in    STD_LOGIC;
        	Q, notQ                 : inout STD_LOGIC 
        );
end RSMS;
										
architecture entity_behavior of RSMS is
signal outdd1, outdd2, outQrs1, outnQrs1, invout, dd3out, dd4out: STD_LOGIC;
begin             
        dd1: entity nand3(default) port map (S, C, C, outdd1);
        dd2: entity nand3(default) port map (R, C, C, outdd2);
        rs1: entity async_rs(default) port map (outdd1, outdd2, outQrs1, outnQrs1);
		inv: entity invertor(invertor) port map(C, invout); 	 	
        dd3: entity nand2(default) port map(invout, outQrs1, dd3out);
        dd4: entity nand2(default) port map(invout, outnQrs1, dd4out);		          
        rs2: entity async_rs(default) port map(dd3out, dd4out, Q, notQ);                 
end entity_behavior; 


architecture component_behavior of RSMS is
signal outdd1, outdd2, outQrs1, outnQrs1, invout, dd3out, dd4out: STD_LOGIC;

        component notand2_gate is
        port(
                        in1, in2                        : in    STD_LOGIC; 
                        out1                            : inout STD_LOGIC
                );
        end component;
        
        component notand3_gate is
        port(
                        in1, in2, in3                     : in    STD_LOGIC;
                        out1                            : inout STD_LOGIC
                );
        end component; 
        
        component invertor_gate is
        port(
		 				input : in STD_LOGIC;
		 				output : out STD_LOGIC
                );
        end component;
		
        component async_rs_gate is
        port(
                        	notS, notR					  	: in    STD_LOGIC;
        					Q, notQ                 		: inout STD_LOGIC
                );
        end component;
        
begin           

		dd1: notand3_gate port map (S, C, C, outdd1);
        dd2: notand3_gate port map (R, C, C, outdd2);
        rs1: async_rs_gate port map (outdd1, outdd2, outQrs1, outnQrs1);
		inv: invertor_gate port map(C, invout); 
        dd3: notand2_gate port map(invout, outQrs1, dd3out);
        dd4: notand2_gate port map(invout, outnQrs1, dd4out);         
        rs2: async_rs_gate port map(dd3out, dd4out, Q, notQ);   

end component_behavior;	   
