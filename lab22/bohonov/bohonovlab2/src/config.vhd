

library	bohonovlab2;

configuration conf of RSMS is 
	for component_behavior
				for all:notand2_gate
                        use entity nand2(default);
                end for; 
                for all:notand3_gate
                        use entity nand3(default);
                end for; 
                for all:invertor_gate
                        use entity invertor(invertor);
                end for; 
                for all:async_rs_gate
                        use entity async_rs(default);
                end for;
        end for;
end conf;				 

