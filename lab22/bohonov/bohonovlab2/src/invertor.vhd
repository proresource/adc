library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity invertor is
	generic(t_rise: time := 0.4 ns; t_fall: time := 0.6 ns);
	 port(
		 input : in STD_LOGIC;
		 output : out STD_LOGIC
	     );
end invertor;

architecture invertor of invertor is
begin  
	process (input) begin
		if(input = '0') then
        	output <= not input after t_rise;
		else									  
			output <= not input after t_fall;
		end if;
	end process;
end invertor;
