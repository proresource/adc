
library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity nand2 is
	generic(t_rise: time := 0.7 ns; t_fall: time := 1.2 ns);
        port (
			in1, in2        : in    STD_LOGIC;
            out1            : inout STD_LOGIC
		);
end nand2;

architecture default of nand2 is   
begin	 										   
	process (in1, in2) begin
		if(in1 = '0' or in2 = '0') then
        	out1 <= not(in1 and in2) after t_rise;
		else									  
			out1 <= not(in1 and in2) after t_fall;
		end if;
	end process;
end default;