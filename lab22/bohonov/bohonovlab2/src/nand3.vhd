library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity nand3 is
        generic(t_rise: time := 0.7 ns; t_fall: time := 1.2 ns);
        port(
        	in1, in2, in3	: in    STD_LOGIC;
            out1            : inout STD_LOGIC
        );
end nand3;

architecture default of nand3 is   
begin   
	process (in1, in2, in3) begin
		if(in1 = '0' or in2 = '0' or in3 = '0') then
        out1 <= not(in1 and in2 and in3) after t_rise;
		else									  
			out1 <= not(in1 and in2 and in3) after t_fall;
		end if;
	end process;
end default;