---------------------------------------------------------------------------------------------------
--
-- Title       : No Title
-- Design      : lab3
-- Author      : ���
-- Company     : ���
--
---------------------------------------------------------------------------------------------------
--
-- File        : D:\VHDL_projects\lab3\lab3\compile\BlockDiagram1.vhd
-- Generated   : Tue May  3 02:10:12 2011
-- From        : D:\VHDL_projects\lab3\lab3\src\BlockDiagram1.bde
-- By          : Bde2Vhdl ver. 2.6
--
---------------------------------------------------------------------------------------------------
--
-- Description : 
--
---------------------------------------------------------------------------------------------------
-- Design unit header --
library IEEE;
use IEEE.std_logic_1164.all;


entity BlockDiagram1 is
  port(
       D : in STD_LOGIC;
       X : in STD_LOGIC;
       mode : in STD_LOGIC;
       Bidirectional0 : inout STD_LOGIC;
       Bidirectional1 : inout STD_LOGIC;
       Q : inout STD_LOGIC
  );
end BlockDiagram1;

architecture BlockDiagram1 of BlockDiagram1 is

---- Signal declarations used on the diagram ----

signal NET209 : STD_LOGIC;

begin

----  Component instantiations  ----

Bidirectional0 <= mode and D;

Bidirectional1 <= X and NET209;

Q <= Bidirectional1 or Bidirectional0;

NET209 <= not(mode);


end BlockDiagram1;
