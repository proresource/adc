library ieee;
use ieee.std_logic_1164.all;

entity shiftRG is
	generic (
		maxL : POSITIVE := 4;
		t_hold : TIME := 15.0 ns
	);
	port(
		clock : in STD_LOGIC;
		mode : in STD_LOGIC;
		D : in STD_LOGIC_VECTOR (maxL downto 0);
		Q : inout STD_LOGIC_VECTOR (maxL downto 0)
	);
end shiftRG;

architecture AshiftRG of shiftRG is
	function posedge (signal s: STD_LOGIC) return STD_LOGIC is begin  
		if s'event=FALSE then
			return '0'; 
		else if s'last_value ='0' and s ='1' then
			return '1';    
	    else if s='0' then
			return '0'; 
        else 
			return 'X'; 
        end if;
		end if;
		end if;
	end posedge;

	begin process(clock) begin				  				   
		if (posedge(clock) = '1') then
			if (mode = '0') then
				Q(0)<= '0' after t_hold;		
				for i in 1 to maxL loop	
					Q(i)<=Q(i-1) after t_hold;
				end loop;
			elsif (mode = '1') then
				for i in 0 to maxL loop
					Q(i)<=D(i) after t_hold;
				end loop;  
			else
				for i in 0 to maxL loop
	  				Q(i) <= 'X' after t_hold;
				end loop;
			end if;		 
		elsif (posedge(clock)='0') then
			null;		
		else 
			for i in 0 to maxL loop
				Q(i) <= 'X' after t_hold;
			end loop;
		end if;
	end process;	

end AshiftRG;

