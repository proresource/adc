library ieee;
use ieee.std_logic_1164.all;

entity shiftrg_tb is
		generic(
		maxL : POSITIVE := 3;
		t_hold : TIME := 15.0 ns
		);
end shiftrg_tb;

architecture TB_ARCHITECTURE of shiftrg_tb is
	component shiftrg
		generic(
		maxL : POSITIVE := 3;
		t_hold : TIME := 15.0 ns
	);
	port(
		clock : in std_logic;
		mode : in std_logic;
		D : in std_logic_vector(maxL downto 0);
		Q : inout std_logic_vector(maxL downto 0) 
	);
	end component;

	-- Stimulus signals 
	signal clock : std_logic;
	signal mode : std_logic;
	signal D : std_logic_vector(maxL downto 0);
	signal Q : std_logic_vector(maxL downto 0);
	shared variable end_sim : BOOLEAN := false;	
begin
	UUT : shiftrg
		generic map (
			maxL => maxL,
			t_hold => t_hold
		)

		port map (
			clock => clock,
			mode => mode,
			D => D,
			Q => Q
		);
		
-------------------------------------------------------------------
--				����������� �������
-------------------------------------------------------------------
	simul_clock	:	process
	begin
		if end_sim = false then	
			clock <= '1';
			wait for 100 ns;
			clock <= '0';
			wait for 100 ns;
		else 
			wait;
		end if;
	end process simul_clock;  
	
	simul_mode	:	process
	begin
		if end_sim = false then	
			mode <= '0';
			wait for 1000 ns;
			mode <= '1';
			wait for 600 ns;
		end if;
	end process simul_mode;

	simul_D	:	process
	begin			   
		if end_sim = false then
			D <= ( 0 => 'Z', 2 => 'U', others => 'X' );
			wait for 1200 ns;
			D <= ( 3 | 1 => '1', others => '0');
			wait for 100 ns;
			D <= ( 3 | 0 => '1', others => '0');
			wait for 300 ns;
		end if;
	end process simul_D;
	
	Clock_Sync	:	process
	constant tsu : time := 10 ns;
	constant thold	 : time := 5 ns;
	begin    	
		wait until rising_edge(clock);
			if (mode = '1') then
				assert (D'stable (tsu))          
					report("D in - Set up time error" & time'image(Now))     
					severity warning;
				wait for thold;
				assert (D'stable(thold))		
					report ("D in - Hold time error" & time'image(Now))
					severity warning; 
			end if;
	end process Clock_Sync;

	
end TB_ARCHITECTURE;

configuration TESTBENCH_FOR_shiftrg of shiftrg_tb is
	for TB_ARCHITECTURE
		for UUT : shiftrg
			use entity work.shiftrg(ashiftrg);
		end for;
	end for;
end TESTBENCH_FOR_shiftrg;

