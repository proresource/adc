SetActiveLib -work
comp -include "$DSN\src\LeftShift.vhd" 
comp -include "$DSN\src\TestBench\shiftrg_TB.vhd" 
asim TESTBENCH_FOR_shiftrg 
wave 
wave -noreg X
wave -noreg clock
wave -noreg mode
wave -noreg D
wave -noreg Q
run 1600 ns;
# The following lines can be used for timing simulation
# acom <backannotated_vhdl_file_name>
# comp -include "$DSN\src\TestBench\shiftrg_TB_tim_cfg.vhd" 
# asim TIMING_FOR_shiftrg 
